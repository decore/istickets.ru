$(document).ready(function(){
    // function, that update the links
    refresh = function(){
        var i = 0;
        $('#partners li a').each(function(){
            $(this).attr('href', img[i][0]);
            $(this).find('img').attr('src', img[i][1]);
            i++;
        });
    }
    // fill array
    var img = [];
    $('#partners li a').each(function(){
        img[img.length] = (
            [
                $(this).attr('href'),
                $(this).find('img').attr('src')
            ]
        );
    });
    // img = img.slice(1, -1); // delete fist and last links
    // clicks
    $('#partners #next').click(function(e){
        e.preventDefault();
        var i = img.shift();
        img.push(i);
        refresh();
    });
    $('#partners #prev').click(function(e){
        e.preventDefault();
        var i = img.pop();
        img.unshift(i);
        refresh();
    });
});