// Window load event used just in case window height is dependant upon images
$(document).ready(function(){
  $(window).bind("load", function() { 
               var footerHeight = 0,
             $footer = $("footer");
             
         positionFooter();
         
         function positionFooter() {
         
                  footerHeight = $footer.height();
         
                 if ( ($(document.body).height()+(footerHeight)) < $(window).height()) {
                     //must stick to bottom
                     $footer.css({
                          position: "fixed",
                          bottom: 0,
                          left:0,
                          right:0
                     })
                 } else {
                     $footer.attr("style", "");
                 }
                 
         }

         $(window).resize(positionFooter);
  });
});